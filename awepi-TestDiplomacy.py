# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Houston Move Portland\nB Austin Support A\nC Dallas Move Portland\nD SanAntonio Move Portland\nE Portland Move Dallas\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Portland\nB Austin\nC [dead]\nD [dead]\nE Dallas\n")

    def test_solve_2(self):
        r = StringIO("A A Support B\nB C Move Plotown\nC K Move A\nD Plotown Hold\nE Soup Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Plotown\nE Soup\n")

    def test_solve_3(self):
        r = StringIO("A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n")
    
# ----
# main
# ----


if __name__ == "__main__":
    main()

